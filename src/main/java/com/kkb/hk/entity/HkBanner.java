package com.kkb.hk.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.io.Serializable;

/**
 * banner表(HkBanner)实体类
 *
 * @author guoyuze
 * @since 2021-09-07 21:10:48
 */
@Data
public class HkBanner implements Serializable {
    private static final long serialVersionUID = 550294301675958811L;
    /**
     * 编号
     */

    private Integer bannerId;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String description;
    /**
     * 封面
     */
    private String image;
    /**
     * 跳转地址
     */
    private String url;
    /**
     * 状态;0=上架 1=下架 2=删除
     */
    private Integer status;
    /**
     * 顺序;越小越靠前
     */
    private Integer sort;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private String createdTime;
    /**
     * 更新人
     */
    private String updatedBy;
    /**
     * 更新时间
     */
    private String updatedTime;

    public HkBanner() {
    }

    /**
     * 插入数据
     * @param title
     * @param description
     * @param image
     * @param url
     * @param status
     * @param sort
     * @param createdBy
     * @param createdTime
     * @param updatedBy
     * @param updatedTime
     */
    public HkBanner(String title, String description, String image, String url,
                    Integer status, Integer sort, String createdBy,
                    String createdTime, String updatedBy, String updatedTime) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.url = url;
        this.status = status;
        this.sort = sort;
        this.createdBy = createdBy;
        this.createdTime = createdTime;
        this.updatedBy = updatedBy;
        this.updatedTime = updatedTime;
    }

    /**
     * 更新数据
     * @param title
     * @param description
     * @param image
     * @param url
     * @param status
     * @param sort
     * @param updatedBy
     * @param updatedTime
     */
    public HkBanner(Integer bannerId,String title, String description, String image, String url,
                    Integer status, Integer sort, String updatedBy, String updatedTime) {
        this.bannerId=bannerId;
        this.title = title;
        this.description = description;
        this.image = image;
        this.url = url;
        this.status = status;
        this.sort = sort;
        this.updatedBy = updatedBy;
        this.updatedTime = updatedTime;
    }
}

