package com.kkb.hk.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kkb.hk.dao.HkBannerDao;
import com.kkb.hk.entity.HkBanner;
import com.kkb.hk.entity.page.PageResult;
import com.kkb.hk.service.HkBannerService;
import com.kkb.hk.utils.PageUtils;
import com.kkb.hk.utils.StringUtils;
import com.kkb.hk.vo.request.banner.HkBannerRequest;
import com.kkb.hk.vo.response.banner.HkBannerResponse;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @className HkBannerServiceImpl
 * @description:  banner表(HkBanner)表服务实现类
 * @author Allen
 * @date 2021/12/16 15:57
 */
@Service("hkBannerService")
public class HkBannerServiceImpl implements HkBannerService {
    @Resource
    private HkBannerDao hkBannerDao;
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * @description:  查询banner列表
     * @param: [hkBannerRequest]
     * @return: java.util.List<com.kkb.hk.vo.response.banner.HkBannerResponse>
     * @author Allen
     * @date: 2021/12/16 19:29
     */
    @Override
    public List<HkBannerResponse> qryList(HkBannerRequest hkBannerRequest) {
        //此处代码需要先从redis中获取，获取不到则取查数据库
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<HkBannerResponse>(HkBannerResponse.class));
        String hk_key="HKBANNER:"+hkBannerRequest.getTitle();
        HkBannerResponse hkBannerResponse=null;
        if (hkBannerRequest.getTitle()!=null && hkBannerRequest.getTitle()!=""){
            hkBannerResponse=(HkBannerResponse) redisTemplate.opsForValue().get(hk_key);
            List<HkBannerResponse> list=new ArrayList<>();
            list.add(hkBannerResponse);
            //System.out.println(hkBannerResponse);
            return list;
        }


        hkBannerRequest.getTitle();
        String historyJsON = null;
        if (StringUtils.isEmpty(historyJsON)) {
            //缓存中没有数据，查询数据库
            List<HkBannerResponse> list = hkBannerDao.qryList(hkBannerRequest);
            //如果数据库中也没有数据
            if (list==null){
                //防止缓存穿透,将这个Title存入redis
                redisTemplate.opsForValue().set(hk_key, com.kkb.hk.vo.response.banner.HkBannerResponse.defaultHkBannerResponse());
            }else {
                //此处代码需要把查出来的结果set redis缓存
                for (HkBannerResponse hk:list){
                    redisTemplate.opsForValue().set(hk_key,hk);
                }
            }

            return list;
        }
        List<HkBannerResponse> list = JSON.parseObject(historyJsON, new TypeReference<List<HkBannerResponse>>() {});
        return list;
    }

    /**
     * @description:  查询banner列表分页查询
     * @param: [hkBannerRequest]
     * @return: com.kkb.hk.entity.page.PageResult
     * @author Allen
     * @date: 2021/12/16 17:48
     */
    @Override
    public PageResult qryListByPage(HkBannerRequest hkBannerRequest) {
        int pageNum = hkBannerRequest.getPageNum();
        int pageSize = hkBannerRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<HkBannerResponse> responseList = this.hkBannerDao.qryListByPage(hkBannerRequest);
        return PageUtils.getPageResult(new PageInfo<HkBannerResponse>(responseList));
    }

    /**
     *
     * @param hkBanner
     * @return
     */
    @Override
    public Integer addAll(HkBanner hkBanner) {
        return hkBannerDao.addBanner(hkBanner);
    }

    /**
     *
     * @param bannerId
     * @return
     */
    @Override
    public int delBannerByBannerId(Integer bannerId) {
        return hkBannerDao.delBannerByBannerId(bannerId);
    }

    /**
     *
     * @param hkBanner
     * @return
     */
    @Override
    public int editBanner(HkBanner hkBanner) {
        return hkBannerDao.editBanner(hkBanner);
    }

}
