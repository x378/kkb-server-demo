package com.kkb.hk.controller;

import com.kkb.hk.entity.HkBanner;
import com.kkb.hk.service.HkBannerService;
import com.kkb.hk.utils.DateUtils;
import com.kkb.hk.utils.ReqResultUtil;
import com.kkb.hk.vo.request.banner.HkBannerRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @className HkBannerController
 * @description:banner接口层
 * @author Allen
 * @date 2021/12/16 15:48
 */
//@RestController
@RequestMapping("hkBanner")
@Slf4j
@Controller
public class HkBannerController {


    @Resource
    private HkBannerService hkBannerService;

    /**
     * @description:  查询banner列表
     * @param: []
     * @return: org.springframework.http.ResponseEntity<java.lang.String>
     * @author Allen
     * @date: 2021/12/16 15:48
     */
    @RequestMapping(value = "/qryList", method = RequestMethod.POST)
    public ResponseEntity<String> qryList(HkBannerRequest hkBannerRequest) {
        log.info("进入banner列表接口");
        log.info("开始");
        return ReqResultUtil.genSuccessResultResponse(hkBannerService.qryList(hkBannerRequest));

    }

    /**
     * @description:分页查询banner列表
     * @param: [hkBannerRequest]
     * @return: org.springframework.http.ResponseEntity<java.lang.String>
     * @author Allen
     * @date: 2021/12/16 18:53
     */
    @RequestMapping(value = "/qryListByPage", method = RequestMethod.POST)
    public ResponseEntity<String> qryListByPage(HkBannerRequest hkBannerRequest) {
        log.info("进入banner列表接口");
        return ReqResultUtil.genSuccessResultResponse(hkBannerService.qryListByPage(hkBannerRequest));
    }

    /**
     * @description:往banner列表插入数据
     * @param request
     * @return
     */
    @RequestMapping("/addHkBanner")
    public String add(HttpServletRequest request){
        request.setAttribute("msg","添加成功");
        HkBanner hkBanner=new HkBanner("标题1","无","无","www.baodu.com",0,1,
                "xfb", DateUtils.getTime(),"xfb",DateUtils.getTime());
        hkBannerService.addAll(hkBanner);
        return "result";
    }

    /**
     * 删除banner表中字段
     * @param request
     * @return
     */
    @RequestMapping("/delBanner")
    public String del(HttpServletRequest request){
        request.setAttribute("msg","删除成功");
        Integer id=1;
        hkBannerService.delBannerByBannerId(id);
        return "result";
    }

    /**
     * 更新banner表中字段
     * @param request
     * @return
     */
    @RequestMapping("/update")
    public String update(HttpServletRequest request){
        request.setAttribute("msg","更新成功");
        HkBanner hkBanner=new HkBanner(2,"标题2","无","无","www.baodu.com",1,
                1,"lisi",DateUtils.getTime());
        hkBannerService.editBanner(hkBanner);
        return "result";
    }

}

